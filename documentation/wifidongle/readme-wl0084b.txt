hab den wl0084b seit Jahren im Einsatz ...

allerdings hab ich nur einen langsamen Router ( Tenda 3G185)
mit maximal 21.6 Mbps .

der wl0084e funktioniert vielleicht auch und da er geleuchtet
hat, sollte man ihn einfach mit einem "langsameren" Netz 
ausprobieren 

der zweite logische Schritt wäre eine manuell Konfiguration
bzw. Installation der firmware (siehe firmware-install.txt)

wifi bzw. wlan-optionen sind sicher auf raspi-config 

    sudo raspi-config 

bzw. manuelle Konfiguration Datei /etc/network/interfaces
(siehe Datei Konfiguration.txt) 