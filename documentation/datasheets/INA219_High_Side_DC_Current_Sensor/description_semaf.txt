INA219 High Side DC Current Sensor Breakout - 26V +/-3.2A Max
SKUSER00525 Hersteller Sonstige
Many current measuring devices are only good for low side measuring and monitoring. 
This means that unless you want to get a battery involved, you have to place the 
measurement resistor between the target ground and true ground. Doing this can cause
problems with circuits since electronics tend to not like it when the ground references 
change and move with varying current draw. It can handle high side current measuring, 
up to +26 VDC, even though it is powered by 3.3 or 5 V. It will also report back the
high side voltage, which is good for tracking battery life or solar panel output.

http://sem.af/qnthl7
 
Many current measuring devices are only good for low side measuring and monitoring
This means that unless you want to get a battery involved, you have to place the 
measurement resistor between the target ground and true ground Doing this can cause
problems with circuits since electronics tend to not like it when the ground references
change and move with varying current draw It can handle high side current measuring,
up to +26 VDC, even though it is powered by 3.3 or 5 V It will also report back the 
high side voltage, which is good for tracking battery life or solar panel output.

A precision amplifier measures the voltage across the 0.1 ohm, 1% sense resistor 
Since the amplifier maximum input difference is +-320mV this means that it can measure 
up to +-3.2 Amps With the internal 12-bit ADC, the resolution at +-3.2 A range is 0.8 mA 
If you were to set the internal gain to the minimum of div8, the max current would be
+-400mA and the resolution would be 0.1 mA Advanced users can remove the 0.1. ohm 
current sense resistor and replace it with their own to change the range, for example 
a 0.01 ohm resistor to measure up to 32 Amps with a resolution of 8mA.

Key Features:

High-side voltage measuring enables battery and solar panel monitorin
Wide operating voltage while protecting your microcontroller and communicating at a 3.3 or 5 V level
Swappable current sensing resistor allows custom current sensing values
Application Ideas:

Solar Panel Output Monitoring
Project Battery Life Monitor
Power Management