  var startTime = new Date().getTime();
  var deltaMouse = 0;
  var lastXPos = 0;
  var lastYPos = 0;
  document.onmousemove = getMousePos;

  function submitForm(formid) {
    var elapsedTime = (new Date().getTime() - startTime) / 1000;
    document.getElementById("elapsedTime").value = elapsedTime;
    document.getElementById("mouseDistance").value = deltaMouse;
  }

  function getMousePos(e) {
    var x = e.pageX;
    var y = e.pageY;

    // Initial
    if(lastXPos==0 && lastYPos==0) {
      lastXPos = x;
      lastYPos = y;
    }
    
    deltaMouse += Math.sqrt(Math.pow(x - lastXPos, 2) + Math.pow(y - lastYPos, 2));
    
    lastXPos = x;
    lastYPos = y;
  }
