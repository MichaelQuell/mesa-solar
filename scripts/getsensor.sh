# the list of these scripts has to be in the same order as in the scripts, that read from cursensor.dat
# this shell script always recreates cursensor.dat (it is an intermediate file with one data record)

cd /home/pi/ioga/mesa-solar/scripts
./01_temperature/01_temperature > cursensor.dat
./02_humidity/02_humidity >> cursensor.dat
python ./06_ADC_Converter/ADC_mcp3008.py >> cursensor.dat 
python 04ir.py >> cursensor.dat 
python 05uv.py >> cursensor.dat 

# preventing i2c_display from demanding input
# forever
echo "Error_File_End" >> cursensor.dat
echo "Error_File_End" >> cursensor.dat
echo "Error_File_End" >> cursensor.dat
echo "Error_File_End" >> cursensor.dat
echo "Error_File_End" >> cursensor.dat
echo "Error_File_End" >> cursensor.dat


