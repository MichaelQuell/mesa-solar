import RPi.GPIO as GPIO
from time import sleep

# The script as below using BCM GPIO 00..nn numbers
GPIO.setmode(GPIO.BCM)

# Set relay pin as output
GPIO.setup(23, GPIO.OUT)

temp = raw_input()
rh = raw_input()
uv = raw_input()
    
    # add here additional sensors
    #ir = raw_input()
    #uv = raw_input()
    #visible = raw_input()
    
# todo: set realistic bounaries
if uv > 800:
    # Turn relay ON
    GPIO.output(23, GPIO.HIGH)
else:
    # Turn all relays OFF
    GPIO.output(23, GPIO.LOW)
