import time
import RPi.GPIO as GPIO

# RPi.GPIO Layout verwenden (wie Pin-Nummern)
GPIO.setmode(GPIO.BOARD)

# Turn off RuntimeWarning This channel is already in use
GPIO.setwarnings(False)

# Pin 15 (GPIO 22) auf Input setzen
GPIO.setup(15, GPIO.IN)

# Pin 16 (GPIO 23) auf Output setzen
GPIO.setup(16, GPIO.OUT)

# Dauersschleife
while 1:
  # LED immer ausmachen
  GPIO.output(16, GPIO.LOW)

  # GPIO lesen
  if GPIO.input(15) == GPIO.HIGH:
    # LED an
    GPIO.output(16, GPIO.HIGH)

    # Warte 10 s
    time.sleep(10)

    # LED aus
    GPIO.output(16, GPIO.LOW)

    # Warte 10 s
    time.sleep(10)