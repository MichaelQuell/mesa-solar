
The unix shell has quite an useful feature which suits to transmit data to our i2c_display.py or 
LCD display program , which is supposed to run in background. (( @reboot option in crontab ))

The renamed am2301.c for temperature and humidity - now 01_temperature_humidity.c  is 
compiled and run with crontab with following options : 

	sensor1=$(01_temperature_humidity)
        01_temperature_humidity >> $currentDate 

A performed dummy test of the above has been successful. 
The $currentDate variable has to be set. Another cronjob is needed for this ,
which of course to be executed every day. 

The required data is transmitted to the LCD display program currently i2c_display.py
the following way 

	python i2c_display.py $sensor1 $sensor2 

$sensor2 is to be reserved for the coming light-sensor which is not implemented yet. 