mesaSolar coding group (mscg) is part of the project mesaSolar. mesaSolar is a PV-driven charging station for mobile phones with free wifi and sitting opportunities. The main goal of mscg is to develop IoT solutions for mesaSolar.

- Links to important tools can be found under links.txt on owncloud
- All relevant information on mesaSolar is gathered in mastersheet.xls on owncloud: ComponentsCostEstimate and mscg_memberslist

Current solutions being worked on (currently on ice in brackets): environmental monitoring, (raspberry pi monitoring), PV system monitoring - state of charge, LED lighting, (wifi usage tracking)
